<?php


namespace Arielpilihanku\Apiatogen;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ApiatoGenCommand extends Command
{

//    const PATH_TEMPLATE = 'vendor/arielpilihanku/apiatogen/src/templates';

    const PATH_TEMPLATE = 'packages/arielpilihanku/apiatogen/src/templates';

    protected $signature = 'apiatogen:run';

    protected $description = 'Generate field for projects apiato';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $container = $this->ask('Enter the name of container');

        $table     = $this->ask('Enter the name of table');

        $this->info('Your container want to generate ' . $container . ' and table ' . $table);

        if ($this->confirm('You wan generate this table now?')) {

            $data = $this->getData($table);

            if (empty($data)) {

                $this->info('Quit without process table not found.');

            } else {

                $fields = $this->mapFields($data);

                $validations = $this->mapValidations($data);

                $transformer = $this->mapTransform($data);

                $sFields =  "'" . join("', \n'" , $fields) . "'";

                $fieldsRequest = $this->mapFieldsRequest($data);

                $sFieldsMap =  join(", \n" , $fieldsRequest);

                $action = $this->getActionContent();

                $actionContent = $this->replaceKeyword($action, $container, $sFieldsMap);

                $actionPath = app_path('Containers/' . $container . '/Actions/' . 'Create'.$container.'Action.php');

                $this->createFile($actionPath, $container, $actionContent);

                $actionUpdate = $this->getActionUpdateContent();

                $actionUpdateContent = $this->replaceKeyword($actionUpdate, $container, $sFieldsMap);

                $actionUpdatePath = app_path('Containers/' . $container . '/Actions/' . 'Update'.$container.'Action.php');

                $this->createFile($actionUpdatePath, $container, $actionUpdateContent);

                $model = $this->getModelContent();

                $modelContent = $this->replaceKeyword($model, $container, $sFields, $table);

                $modelPath = app_path('Containers/' . $container . '/Models/' . $container.'.php');

                $this->createFile($modelPath, $container, $modelContent);

                $requestCreate = $this->getRequestCreateContent();

                $requestCreateContent = $this->replaceKeyword($requestCreate, $container, $sFields, $table, $validations);

                $requestCreatePath = app_path('Containers/' . $container . '/UI/API/Requests/' . 'Create'. $container .'Request.php');

                $this->createFile($requestCreatePath, $container, $requestCreateContent);

                $requestUpdate = $this->getRequestUpdateContent();

                $requestUpdateContent = $this->replaceKeyword($requestUpdate, $container, $sFields, $table, $validations);

                $requestUpdatePath = app_path('Containers/' . $container . '/UI/API/Requests/' . 'Update'. $container .'Request.php');

                $this->createFile($requestUpdatePath, $container, $requestUpdateContent);

                $requestDelete = $this->getRequestDeleteContent();

                $requestDeleteContent = $this->replaceKeyword($requestDelete, $container, $sFields, $table, $validations);

                $requestDeletePath = app_path('Containers/' . $container . '/UI/API/Requests/' . 'Delete'. $container .'Request.php');

                $this->createFile($requestDeletePath, $container, $requestDeleteContent);

                $requestFind = $this->getRequestFindContent();

                $requestFindContent = $this->replaceKeyword($requestFind, $container, $sFields, $table, $validations);

                $requestFindPath = app_path('Containers/' . $container . '/UI/API/Requests/' . 'Find'. $container . 'ByIdRequest.php');

                $this->createFile($requestFindPath, $container, $requestFindContent);

                $requestAll = $this->getRequestAllContent();

                $requestAllContent = $this->replaceKeyword($requestAll, $container, $sFields, $table, $validations);

                $requestAllPath = app_path('Containers/' . $container . '/UI/API/Requests/' . 'GetAll'. $container .'sRequest.php');

                $this->createFile($requestAllPath, $container, $requestAllContent);

                $transform = $this->getTransformContent();

                $transformContent = $this->replaceKeyword($transform, $container, $sFields, null, null, $transformer);

                $transformPath = app_path('Containers/' . $container . '/UI/API/Transformers/' . $container .'Transformer.php');

                $this->createFile($transformPath, $container, $transformContent);

                $controller = $this->getControllerContent();

                $controllerContent = $this->replaceKeyword($controller, $container, $sFields, $table, $validations);

                $controllerPath = app_path('Containers/' . $container . '/UI/API/Controllers/Controller.php');

                $this->createFile($controllerPath, $container, $controllerContent);

                $this->info('Done.');

            }

        } else {

            $this->info('Quit without process.');

        }

    }

    public function getData($table)
    {
        $raw = "SELECT COLUMN_NAME as field, CHARACTER_MAXIMUM_LENGTH as length, IS_NULLABLE AS required
                FROM information_schema.columns where table_schema = '" . env('DB_DATABASE') . "' AND TABLE_NAME IN ('" . $table ."')
                AND COLUMN_NAME NOT IN ('id', 'password', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_at')";

        return $result = DB::select($raw);
    }

    public function mapFields($data)
    {
        return array_map(function ($obj) {

            return $dt[] = $obj->field;


        }, $data);

    }

    public function mapFieldsRequest($data)
    {
        return array_map(function ($obj) {

            return $dt[] = "'" . lcfirst(str_replace('_', '', ucwords($obj->field, '_'))) . "'";

        }, $data);

    }

    public function mapValidations($data)
    {
        $validation = "";

        foreach ($data as $object) {

            if ($object->required == 'YES' && $object->length != null) {
                $vd = "'max:" . $object->length . "'";
            } elseif ($object->required != 'YES' && $object->length == null) {
                $vd = "'required'";
            } elseif ($object->required != 'YES' && $object->length != null) {
                $vd = "'required|max:" . $object->length . "'";
            } else {
                $vd = "''";
            }

            $validation .= "'" . lcfirst(str_replace('_', '', ucwords($object->field, '_'))) . "'" . "=>" . $vd . ",\n";

        }

        return $validation;

    }

    public function mapTransform($data)
    {
        $transform = "";

        foreach ($data as $object) {

            $transform .= "'" . lcfirst(str_replace('_', '', ucwords($object->field, '_'))) . "'" . "=> $" . "entity->" . $object->field . ",\n";

        }

        return $transform;

    }

    protected function replaceKeyword($content, $container, $fields, $table = null, $validations = null, $transform = null)
    {
        return preg_replace(array(
            '#_Container_#',
            '#_Fields_#',
            '#_table_#',
            '#_validations_#',
            '#_fieldTransformer_#',
            '#_Permissions_#',
        ), array(
            $container,
            $fields,
            $table,
            $validations,
            $transform,
            str_replace('_', '-', $table)
        ), $content);
    }

    protected function getActionContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/actions/create.txt'));
    }

    protected function getActionUpdateContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/actions/update.txt'));

    }

    protected function getModelContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/model/file.txt'));

    }

    protected function getRequestCreateContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/requests/create.txt'));

    }

    protected function getRequestUpdateContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/requests/update.txt'));
    }

    protected function getRequestFindContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/requests/find.txt'));
    }

    protected function getRequestAllContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/requests/all.txt'));
    }

    protected function getRequestDeleteContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/requests/delete.txt'));
    }

    protected function getTransformContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/transforms/file.txt'));
    }

    protected function getControllerContent()
    {
        return file_get_contents(base_path(self::PATH_TEMPLATE . '/controllers/file.txt'));
    }

    protected function checkDir($dir)
    {
        if (!File::isDirectory($dir)) {

            File::makeDirectory($dir);

        }
    }

    public function createFile($filePath, $container, $content)
    {
        $this->checkDir(app_path('Containers'));

        $this->checkDir(app_path('Containers/' . $container));

        $this->checkDir(app_path('Containers/' . $container . '/Actions'));

        $this->checkDir(app_path('Containers/' . $container . '/Models'));

        $this->checkDir(app_path('Containers/' . $container . '/UI'));

        $this->checkDir(app_path('Containers/' . $container . '/UI/API'));

        $this->checkDir(app_path('Containers/' . $container . '/UI/API/Requests'));

        $this->checkDir(app_path('Containers/' . $container . '/UI/API/Transformers'));

        $this->checkDir(app_path('Containers/' . $container . '/UI/API/Controllers'));

        if (file_exists($filePath)) {

            unlink($filePath);

        }

        $fp = fopen($filePath, 'a') or die("CAN'T CREATE FILE: " . $filePath);
        fwrite($fp, $content);

        fclose($fp);

    }


}
