<?php

namespace Arielpilihanku\Apiatogen;

use Illuminate\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;

class ApiatoGenServiceProvider extends ServiceProvider {

    protected $commands = [
        'Arielpilihanku\Apiatogen\ApiatoGenCommand',
    ];

    public function register(){
        $this->commands($this->commands);
    }
}

